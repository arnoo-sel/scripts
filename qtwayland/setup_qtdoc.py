#!/usr/bin/python3

import argparse
import os
import sys

def exec_cmd(cmd):
    ret = os.system(cmd)
    if (ret == 0):
        return True
    else:
        print("\033[01;31mError installing the documentation. The following command failed: \n--    " + cmd)
        print("Abort.\033[00m")
        sys.exit(-1)

def print_info(str):
    print("\033[01;36m" + str + "\033[00m")
        

parser = argparse.ArgumentParser(description='Generate and register the Qt documentation')

parser.add_argument('--no-register', action='store_const', const=True, default=False)
parser.add_argument('--no-generate',  action='store_const', const=True, default=False)
parser.add_argument('--src-dir', default=os.getcwd() + "/qt_src")
parser.add_argument('--assistant-path', default=os.getcwd() + "/qt_dir/bin/assistant")

ret = parser.parse_args()

if (ret.no_generate == False):
    print_info("Generating the documentation")
    exec_cmd("cd " + ret.src_dir + " && make -j 8 docs")

if (ret.no_register == False):
    print_info("Registering the documention")
    exec_cmd('bash -c \' for file in `find ' + ret.src_dir + '/ -name "*qt*.qch" -print`; do ' + ret.assistant_path + ' -register $file; done \'')

print("\033[01;32m" + "Done." + "\033[00m")
