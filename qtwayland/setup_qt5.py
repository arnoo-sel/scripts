#!/usr/bin/python3

import argparse
import os
import sys

def exec_cmd(cmd):
    ret = os.system(cmd)
    if (ret == 0):
        return True
    else:
        print("\033[01;31mError installing Qt5. The following command failed: \n--    " + cmd)
        print("Abort.\033[00m")
        sys.exit(-1)

def print_info(str):
    print("\033[01;36m" + str + "\033[00m")
        

parser = argparse.ArgumentParser(description='Install a QtWayland-compatible Qt5 copy')

parser.add_argument('--no-webkit', action='store_const', const=True, default=False)
parser.add_argument('--no-dl',  action='store_const', const=True, default=False)
parser.add_argument('--src-dir', default=os.getcwd() + "/qt_src")
parser.add_argument('--install-dir', default=os.getcwd() + "/qt_dir")
parser.add_argument('--qt-version', default="git")
parser.add_argument('--configure-options', default=["-opensource", "-nomake tests", "-confirm-license", "-developer-build", "-no-gtkstyle", "-egl", "-opengl es2"], nargs=argparse.REMAINDER)

ret = parser.parse_args()

if (ret.no_dl == False):
    if (ret.qt_version == 'git'):
        print_info("Downloading Qt from gitorious repository")
        exec_cmd("git clone git://gitorious.org/qt/qt5.git " + ret.src_dir)

        repo_init_str = "perl init-repository"
        if (ret.no_webkit == True):
            repo_init_str += " --no-webkit"

        print_info("Initiating repository, downloading Qt modules")
        exec_cmd("cd " + ret.src_dir + " && " + repo_init_str)

print_info("Configuring Qt")
exec_cmd("cd " + ret.src_dir + " && ./configure " + " ".join(ret.configure_options) + " -prefix " + ret.install_dir)

print_info("Building Qt")
exec_cmd("cd " + ret.src_dir + " && make -j 8")

print_info("Installing Qt")
exec_cmd("cd " + ret.src_dir + " && make install")

print("\033[01;32m" + "Done." + "\033[00m")
