#!/usr/bin/python3

import argparse
import os
import sys

def exec_cmd(cmd):
    ret = os.system(cmd)
    if (ret == 0):
        return True
    else:
        print("\033[01;31mError installing QtWayland. The following command failed: \n--    " + cmd)
        print("Abort.\033[00m")
        sys.exit(-1)

def print_info(str):
    print("\033[01;36m" + str + "\033[00m")
        

parser = argparse.ArgumentParser(description='Install QtWayland')

parser.add_argument('--no-qtcompositor', action='store_const', const=True, default=False)
parser.add_argument('--no-dl',  action='store_const', const=True, default=False)
parser.add_argument('--qmake-path', default=os.getcwd() + "/qt_dir/bin/qmake")
parser.add_argument('--src-dir', default=os.getcwd() + "/qtwayland_src")
parser.add_argument('--qtwayland-version', default="git")

ret = parser.parse_args()

if (ret.no_dl == False):
    if (ret.qtwayland_version == 'git'):
        print_info("Downloading QtWayland from git repository")
        exec_cmd("git clone git://gitorious.org/qt/qtwayland.git " + ret.src_dir)

        print_info("Patching QtWayland")
        exec_cmd("patch " + ret.src_dir + "/src/plugins/platforms/qwayland-egl/qwaylandeglwindow.cpp qtwayland.patch")

qmake_cmd = ret.qmake_path
if (ret.no_qtcompositor == False):
    qmake_cmd += " CONFIG+=wayland-compositor"
print_info("Running qmake")
exec_cmd("cd " + ret.src_dir + " && " + qmake_cmd)

print_info("Building QtWayland")
exec_cmd("cd " + ret.src_dir + " && make -j 8")

print_info("Installing QtWayland")
exec_cmd("cd " + ret.src_dir + " && make install")

print("\033[01;32m" + "Done." + "\033[00m")
